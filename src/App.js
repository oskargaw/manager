import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import firebase from 'firebase';
import ReduxThunk from 'redux-thunk';
import reducers from './reducers';
import Router from './Router';

class App extends Component {
  componentWillMount() {
    const config = {
      apiKey: 'AIzaSyCLUI_9-woR52dQNunX5xwLSaNaVEh1Nq8',
      authDomain: 'manager-dfcba.firebaseapp.com',
      databaseURL: 'https://manager-dfcba.firebaseio.com',
      storageBucket: 'manager-dfcba.appspot.com',
      messagingSenderId: '796964592003'
  };

  firebase.initializeApp(config);
}
  render() {
    const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));


    return (
      <Provider store={store}>
          <Router />
      </Provider>
    );
  }
}

export default App;
